<?php
namespace app\assets;

use yii\web\AssetBundle;

class FontFamilyAsset extends AssetBundle{
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//font.googleapis.com/css2?family=Roboto&display=swap'
            ];
    public $cssOptions = [
        'type' => 'text/css',
    ];
}
