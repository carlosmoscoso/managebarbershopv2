<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property int $IDcitas
 * @property string $fecha
 * @property string $hora
 * @property float $coste
 * @property string $categoria
 * @property int|null $IDclientes_citas
 *
 * @property Clientes $iDclientesCitas
 * @property Productos[] $productos
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'coste', 'categoria'], 'required'],
            [['fecha', 'hora'], 'safe'],
            [['coste'], 'number'],
            [['IDclientes_citas'], 'integer'],
            [['categoria'], 'string', 'max' => 50],
            [['IDclientes_citas'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes_citas' => 'IDcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDcitas' => 'I Dcitas',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'coste' => 'Coste',
            'categoria' => 'Categoria',
            'IDclientes_citas' => 'I Dclientes Citas',
        ];
    }

    /**
     * Gets query for [[IDclientesCitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientesCitas()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDclientes_citas']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['IDclientes_productos' => 'IDcitas']);
    }
}
