<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IDprod
 * @property string $nombre
 * @property float $costeCliente
 * @property float $costeProveedores
 * @property string $categoria
 * @property string $metodoPago
 * @property int|null $IDproveedores_productos
 * @property int|null $IDclientes_productos
 *
 * @property Citas $iDclientesProductos
 * @property Proveedores $iDproveedoresProductos
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'costeCliente', 'costeProveedores', 'categoria', 'metodoPago'], 'required'],
            [['costeCliente', 'costeProveedores'], 'number'],
            [['IDproveedores_productos', 'IDclientes_productos'], 'integer'],
            [['nombre', 'categoria', 'metodoPago'], 'string', 'max' => 50],
            [['IDclientes_productos'], 'exist', 'skipOnError' => true, 'targetClass' => Citas::className(), 'targetAttribute' => ['IDclientes_productos' => 'IDcitas']],
            [['IDproveedores_productos'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['IDproveedores_productos' => 'IDproveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDprod' => 'I Dprod',
            'nombre' => 'Nombre',
            'costeCliente' => 'Coste Cliente',
            'costeProveedores' => 'Coste Proveedores',
            'categoria' => 'Categoria',
            'metodoPago' => 'Metodo Pago',
            'IDproveedores_productos' => 'I Dproveedores Productos',
            'IDclientes_productos' => 'I Dclientes Productos',
        ];
    }

    /**
     * Gets query for [[IDclientesProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientesProductos()
    {
        return $this->hasOne(Citas::className(), ['IDcitas' => 'IDclientes_productos']);
    }

    /**
     * Gets query for [[IDproveedoresProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproveedoresProductos()
    {
        return $this->hasOne(Proveedores::className(), ['IDproveedor' => 'IDproveedores_productos']);
    }
}
