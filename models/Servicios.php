<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicios".
 *
 * @property int $IDserv
 * @property string $nombre
 * @property float $coste
 * @property string $categoria
 * @property int|null $IDclientes_servicios
 *
 * @property Clientes $iDclientesServicios
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'coste', 'categoria'], 'required'],
            [['coste'], 'number'],
            [['IDclientes_servicios'], 'integer'],
            [['nombre', 'categoria'], 'string', 'max' => 50],
            [['IDclientes_servicios'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes_servicios' => 'IDcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDserv' => 'I Dserv',
            'nombre' => 'Nombre',
            'coste' => 'Coste',
            'categoria' => 'Categoria',
            'IDclientes_servicios' => 'I Dclientes Servicios',
        ];
    }

    /**
     * Gets query for [[IDclientesServicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientesServicios()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDclientes_servicios']);
    }
}
