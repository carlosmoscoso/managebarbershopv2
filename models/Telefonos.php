<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $IDtel
 * @property int|null $IDclientes_telefonos
 * @property string $teléfonos
 *
 * @property Clientes $iDclientesTelefonos
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDclientes_telefonos'], 'integer'],
            [['teléfonos'], 'required'],
            [['teléfonos'], 'string', 'max' => 9],
            [['IDclientes_telefonos'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes_telefonos' => 'IDcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDtel' => 'I Dtel',
            'IDclientes_telefonos' => 'I Dclientes Telefonos',
            'teléfonos' => 'Teléfonos',
        ];
    }

    /**
     * Gets query for [[IDclientesTelefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientesTelefonos()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDclientes_telefonos']);
    }
}
