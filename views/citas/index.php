<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Citas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Citas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDcitas',
            'fecha',
            'hora',
            'coste',
            'categoria',
            //'IDclientes_citas',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Citas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDcitas' => $model->IDcitas]);
                 }
            ],
        ],
    ]); ?>


</div>
