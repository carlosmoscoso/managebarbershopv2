<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = $model->IDcitas;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="citas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDcitas' => $model->IDcitas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDcitas' => $model->IDcitas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDcitas',
            'fecha',
            'hora',
            'coste',
            'categoria',
            'IDclientes_citas',
        ],
    ]) ?>

</div>
