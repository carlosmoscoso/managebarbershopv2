<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\Icon;
Icon::map($this, Icon::FA);
AppAsset::register($this);
use kartik\bs4dropdown\Dropdown;




?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/image/manage-redonda.jpg" type="image/x-icon" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>



<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        
        <div class ="row justify-content-center">
            
<div class = "card col-2 m-2"> 
    <div class = "card-body">
        
        <center>
          <img src= "image/homeR.jpg" width="65">
          
          <br>
          <br>
        <p>
            HOME
        </p>
        </center>
    </div>
     </div>
            
<div class = "card col-2 m-2">
    <div class = "card-body">
        
        <center>
        <img src= "image/calendarR.jpg" width="65">
        <br>
        <br>
        <p>
           CITAS
        </p>
        </center>
    </div>
     </div>
            
<div class = "card col-2 m-2">
    <div class = "card-body">
        <center>
        <img src= "image/userR.jpg" width="65">
        <br>
        <br>
        <p>
            CLIENTES
        </p>
        </center>
    </div>
     </div>
    <div class = "card col-2 m-2 ">
    <div class = "card-body ">
        
        <center>
       <img src= "image/documentR.jpg" width="65">
        <br>
        <br>
        <p>
            STOCK
        </p>
        </center>
    </div>
     </div>
    
</div>
        <?= 
            $content 
        ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; MANAGEBARBERSHOP <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
