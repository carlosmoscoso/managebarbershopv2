<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */

$this->title = $model->IDprod;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDprod' => $model->IDprod], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDprod' => $model->IDprod], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDprod',
            'nombre',
            'costeCliente',
            'costeProveedores',
            'categoria',
            'metodoPago',
            'IDproveedores_productos',
            'IDclientes_productos',
        ],
    ]) ?>

</div>
