<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

$this->title = 'Update Telefonos: ' . $model->IDtel;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDtel, 'url' => ['view', 'IDtel' => $model->IDtel]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
